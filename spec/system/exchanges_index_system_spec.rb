require 'rails_helper'

RSpec.describe "Exchange Currency Process", :type => :system, js: true do

    it "exchange value" do
        visit '/'
        within("#exchange_form") do
            select('BRL', from: 'source_currency')
            select('EUR', from: 'target_currency')
            fill_in 'amount', with: '10'
        end
        expect('amount').not_to  be_empty 
    end
end